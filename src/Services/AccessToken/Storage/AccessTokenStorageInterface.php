<?php

namespace Drupal\tracardi\Services\AccessToken\Storage;

use League\OAuth2\Client\Token\AccessToken;

interface AccessTokenStorageInterface {
  public function get(): ?AccessToken;

  public function set(AccessToken $accessToken): void;

  public function clear(): void;
}

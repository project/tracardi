<?php

namespace Drupal\tracardi\Services\AccessToken\Storage;

use Drupal\Core\State\StateInterface;
use League\OAuth2\Client\Token\AccessToken;

final class DrupalStateAccessTokenStorage implements AccessTokenStorageInterface {

  private const STATE_KEY = 'tracardi_access_token';

  private StateInterface $state;

  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  public function get(): ?AccessToken {
    $accessToken = $this->state->get(self::STATE_KEY);
    if ($accessToken === NULL) {
      return NULL;
    }

    $decodedAccessToken = json_decode($accessToken, TRUE);

    return new AccessToken($decodedAccessToken);
  }

  public function set(AccessToken $accessToken): void {
    $encodedAccessToken = json_encode($accessToken);
    $this->state->set(self::STATE_KEY, $encodedAccessToken);
  }

  public function clear(): void {
    $this->state->delete(self::STATE_KEY);
  }
}

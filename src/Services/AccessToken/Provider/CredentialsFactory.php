<?php

namespace Drupal\tracardi\Services\AccessToken\Provider;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;

final class CredentialsFactory {

  private ImmutableConfig $tracardiConfig;

  public function __construct(ImmutableConfig $tracardiConfig) {
    $this->tracardiConfig = $tracardiConfig;
  }

  public function create(): Credentials {
    // @todo: set error if not exists.
    $username = $this->tracardiConfig->get('api_username');
    $password = $this->tracardiConfig->get('api_password');

    return new Credentials($username, $password);
  }
}

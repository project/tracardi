<?php

namespace Drupal\tracardi\Services\AccessToken\Provider;

use Drupal\Core\Config\ImmutableConfig;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\GenericProvider;

final class OAuthProviderFactory {

  private ImmutableConfig $tracardiConfig;

  public function __construct(ImmutableConfig $tracardiConfig) {
    $this->tracardiConfig = $tracardiConfig;
  }

  public function create(): AbstractProvider {
    $baseUrl = $this->tracardiConfig->get('api_url');

    return new GenericProvider([
      'urlAuthorize' => $baseUrl . '/user/authorize',
      'urlAccessToken' => $baseUrl . '/user/token',
      'urlResourceOwnerDetails' => $baseUrl . '/user/resource',
    ]);
  }
}

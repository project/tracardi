<?php

namespace Drupal\tracardi\Services\AccessToken\Context;

use Drupal\tracardi\Services\AccessToken\Storage\AccessTokenStorageInterface;
use League\OAuth2\Client\Token\AccessToken;

final class StorageAccessTokenContext implements AccessTokenContextInterface {

  private AccessTokenStorageInterface $accessTokenStorage;

  public function __construct(AccessTokenStorageInterface $accessTokenStorage) {
    $this->accessTokenStorage = $accessTokenStorage;
  }

  public function getAccessToken(): AccessToken {
    $accessToken = $this->accessTokenStorage->get();
    if ($accessToken !== NULL) {
      return $accessToken;
    }

    throw new CouldNotFetchTokenException();
  }

}

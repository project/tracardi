<?php

namespace Drupal\tracardi\Services\AccessToken\Context;

use Drupal\tracardi\Services\AccessToken\Provider\Credentials;
use Drupal\tracardi\Services\AccessToken\Storage\AccessTokenStorageInterface;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Token\AccessToken;

final class ProviderAccessTokenContext implements AccessTokenContextInterface {

  private AbstractProvider $provider;

  private Credentials $credentials;

  private AccessTokenStorageInterface $accessTokenStorage;

  public function __construct(
    AbstractProvider $provider,
    Credentials $credentials,
    AccessTokenStorageInterface $accessTokenStorage
  ) {
    $this->provider = $provider;
    $this->credentials = $credentials;
    $this->accessTokenStorage = $accessTokenStorage;
  }

  public function getAccessToken(): AccessToken {
    $accessToken = $this->provider->getAccessToken('password', [
      'username' => $this->credentials->getUsername(),
      'password' => $this->credentials->getPassword(),
    ]);
    $this->accessTokenStorage->set($accessToken);

    return $accessToken;
  }

}

<?php

namespace Drupal\tracardi\Services\AccessToken\Context;

use League\OAuth2\Client\Token\AccessToken;

final class ChainAccessTokenContext implements AccessTokenContextInterface {
  /**
   * @var AccessTokenContextInterface[] $tokenContexts
   */
  private array $tokenContexts;

  /**
   * @param AccessTokenContextInterface[] $tokenContexts
   */
  public function __construct(array $tokenContexts) {
    $this->tokenContexts = $tokenContexts;
  }

  public function getAccessToken(): AccessToken {
    foreach ($this->tokenContexts as $accessTokenContext) {
      try {
        return $accessTokenContext->getAccessToken();
      }
      catch (CouldNotFetchTokenException $e) {
        continue;
      }
    }

    throw new CouldNotFetchTokenException('None of the provided contexts was able to retrieve an access token');
  }
}

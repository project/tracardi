<?php

namespace Drupal\tracardi\Services\AccessToken\Context;

use League\OAuth2\Client\Token\AccessToken;

interface AccessTokenContextInterface {

  public function getAccessToken(): AccessToken;

}

<?php

namespace Drupal\tracardi\Services\AccessToken\Context;

final class CouldNotFetchTokenException extends \RuntimeException {
  //
}

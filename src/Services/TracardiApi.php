<?php

namespace Drupal\tracardi\Services;

use Drupal\tracardi\Exception\ProfileUnknownException;
use Tracardi\TracardiPhpSdk\Http\ApiClient\ApiClient;
use Tracardi\TracardiPhpSdk\Model\Profile\Profile;
use Tracardi\TracardiPhpSdk\Model\Segment\Group;
use Tracardi\TracardiPhpSdk\Tracardi;
use Http\Client\Exception\HttpException;

final class TracardiApi {

  private ApiClient $apiClient;

  public function __construct(ApiClient $apiClient) {
    $this->apiClient = $apiClient;
  }

  public function isConnected(): bool {
    try {
      // @TODO Replace with info endpoint.
      return (bool) Tracardi::withDefaultSerializer($this->apiClient)
        ->segments()
        ->listSegments()
        ->getGroups();
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * @return string[]
   */
  public function getSegments(): array {
    try {
      return Tracardi::withDefaultSerializer($this->apiClient)
        ->segments()
        ->listSegments()
        ->getGroups();
    }
    catch (HttpException $e) {
      return [];
    }
  }

  public function getProfile(string $id): Profile {
    try {
      return Tracardi::withDefaultSerializer($this->apiClient)
        ->profiles()
        ->getProfile($id);
    }
    catch (HttpException $e) {
      if ($e->getCode() === 404) {
        throw new ProfileUnknownException();
      }

      throw $e;
    }
  }

}

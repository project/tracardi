<?php

namespace Drupal\tracardi\Services;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Http\ClientFactory;
use GuzzleHttp\HandlerStack;
use Tracardi\TracardiPhpSdk\Http\ApiClient\ApiClient;
use Drupal\tracardi\Services\Middleware\EnsureAccessTokenExistsMiddleware;

final class ApiClientFactory {

  private ImmutableConfig $tracardiConfig;

  private ClientFactory $clientFactory;

  private EnsureAccessTokenExistsMiddleware $ensureAccessTokenExistsMiddleware;

  public function __construct(
    ImmutableConfig $tracardiConfig,
    ClientFactory $client_factory,
    EnsureAccessTokenExistsMiddleware $ensureAccessTokenExistsMiddleware
  ) {
    $this->tracardiConfig = $tracardiConfig;
    $this->clientFactory = $client_factory;
    $this->ensureAccessTokenExistsMiddleware = $ensureAccessTokenExistsMiddleware;
  }

  public function create(): ApiClient {
    $baseUrl = $this->tracardiConfig->get('api_url');
    $stack = $this->getHandlerStack();

    $guzzleClient = $this->clientFactory->fromOptions([
      'base_uri' => $baseUrl,
      'handler' => $stack,
      'timeout' => 3,
      'connect_timeout' => 1,
    ]);

    return ApiClient::withClient($guzzleClient);
  }

  private function getHandlerStack(): HandlerStack {
    $stack = HandlerStack::create();
    $stack->push($this->ensureAccessTokenExistsMiddleware);

    return $stack;
  }
}

<?php

namespace Drupal\tracardi;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Tracardi\TracardiPhpSdk\Model\Profile\Profile;

interface PersonalizationInterface extends ConfigurableInterface, PluginFormInterface {

  /**
   * @return array|null
   */
  public function getPageLibraries(): ?array;

  /**
   * @return array|null
   */
  public function getPageDrupalSettings(): ?array;

  /**
   * @return array|null
   */
  public function getOptionsList(): ?array;

  /**
   * @return array
   */
  public function getPreviewWidget(): array;

  /**
   * @return string|null
   */
  public function getCookieName(): ?string;

  /**
   * @param \Tracardi\TracardiPhpSdk\Model\Profile\Profile $profile
   *
   * @return array
   */
  public function calculate(Profile $profile): array;

  /**
   * @return string
   */
  public function getId(): string;

  /**
   * @return string
   */
  public function label(): string;

  /**
   * @return \Drupal\Component\Render\MarkupInterface
   */
  public function getDescription(): MarkupInterface;

  /**
   * @return bool
   */
  public function isConfigurable(): bool;

  /**
   * @return bool
   */
  public function isEnabled(): bool;

  /**
   * @return string
   */
  public function getConfigName(): string;

}

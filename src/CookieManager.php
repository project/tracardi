<?php

namespace Drupal\tracardi;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class CookieManager {

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(RequestStack $requestStack, ConfigFactoryInterface $configFactory) {
    $this->requestStack = $requestStack;
    $this->config = $configFactory->get('tracardi.settings');
  }

  /**
   * @param string $cookieName
   *
   * @return bool
   */
  public function hasCookie(string $cookieName): bool {
    return $this->requestStack->getCurrentRequest()->cookies->has($cookieName);
  }

  /**
   * @param string $cookieName
   *
   * @return string
   */
  public function getCookieValue(string $cookieName): string {
    return $this->requestStack->getCurrentRequest()->cookies->get($cookieName, '');
  }

  /**
   * @param string $cookieName
   *
   * @return array
   */
  public function getCookieValues(string $cookieName): array {
    $cookieValue = $this->requestStack->getCurrentRequest()->cookies->get($cookieName, '');
    return array_filter(explode(',', $cookieValue));
  }

  /**
   * @return bool
   */
  public function isPersonalized(): bool {
    if (!$this->config->get('ab_testing')) {
      return TRUE;
    }

    $cookieName = $this->config->get('ab_cookie_name');
    return !$this->hasCookie($cookieName) || !filter_var($this->getCookieValue($cookieName), FILTER_VALIDATE_BOOLEAN);
  }

}

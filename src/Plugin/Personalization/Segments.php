<?php

namespace Drupal\tracardi\Plugin\Personalization;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\tracardi\CookieManager;
use Drupal\tracardi\PersonalizationBase;
use Drupal\tracardi\Services\TracardiApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Tracardi\TracardiPhpSdk\Model\Profile\Profile;
use Tracardi\TracardiPhpSdk\Model\Segment\Segment;

/**
 * @Personalization(
 *   id = "segments",
 *   label = "Segment(s)",
 *   description = @Translation("Sets a cookie based on segmentation of the Tracardi profile"),
 *   configurable = TRUE
 * )
 */
class Segments extends PersonalizationBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\tracardi\Services\TracardiApi
   */
  private TracardiApi $tracardiApi;

  /**
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\tracardi\CookieManager $cookieManager
   * @param \Drupal\Core\Session\AccountProxyInterface $accountProxy
   * @param \Drupal\tracardi\Services\TracardiApi $tracardiApi
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CookieManager $cookieManager, AccountProxyInterface $accountProxy, TracardiApi $tracardiApi) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $cookieManager, $accountProxy);

    $this->tracardiApi = $tracardiApi;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   *
   * @return \Drupal\tracardi\Plugin\Personalization\Segments
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): Segments {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('tracardi.cookie_manager'),
      $container->get('current_user'),
      $container->get('tracardi_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getOptionsList(): ?array {
    $segments = [
      '_unknown' => t('- Unknown -'),
    ];

    /** @var \Tracardi\TracardiPhpSdk\Model\Segment\Group $segmentGroup */
    foreach ($this->tracardiApi->getSegments() as $segmentGroup) {
      $groupName = $segmentGroup->getName();
      $segments[$groupName] = $segments[$groupName] ?? [];

      /** @var Segment $segment */
      foreach ($segmentGroup->getSegments() as $segment) {
        $segments[$groupName][$segment->getMachineName()] = $segment->getName();
      }
    }

    return $segments;
  }

  /**
   * {@inheritdoc}
   */
  public function getCookieName(): ?string {
    return $this->configuration['cookie_name'];
  }

  /**
   * {@inheritdoc}
   */
  public function calculate(Profile $profile): array {
    return $profile->getSegments();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'cookie_name' => 'segment',
    ];
  }

  /**
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    return parent::buildConfigurationForm($form, $form_state) + [
      'cookie_name' => [
        '#type' => 'textfield',
        '#title' => $this->t('Cookie name'),
        '#default_value' => $this->configuration['cookie_name'],
        '#required' => TRUE,
      ],
    ];
  }

}

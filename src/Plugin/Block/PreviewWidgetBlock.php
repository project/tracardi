<?php

namespace Drupal\tracardi\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\tracardi\PersonalizationManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *   id = "tracardi_preview_widget",
 *   admin_label = @Translation("Preview widget (Tracardi)"),
 * )
 */
class PreviewWidgetBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\tracardi\PersonalizationManager
   */
  private $personalizationManager;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\tracardi\PersonalizationManager $personalizationManager
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PersonalizationManager $personalizationManager, ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->personalizationManager = $personalizationManager;
    $this->config = $configFactory->get('tracardi.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.personalization'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account): AccessResultInterface {
    $allowed = $this->config->get('preview') && $account->hasPermission('preview personalization');
    return AccessResult::allowedIf($allowed);
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return [
      '#theme' => 'preview_widget',
      '#plugins' => $this->personalizationManager->loadEnabledPlugins(),
      '#attached' => [
        'library' => [
          'tracardi/tracardi_preview'
        ],
      ],
    ];
  }

}

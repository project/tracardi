<?php

namespace Drupal\tracardi\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformInterface;

/**
 * Adds Tracardi data attribute to certain form fields.
 *
 * @WebformHandler(
 *   id = "tracardi",
 *   label = @Translation("Tracardi data attribute"),
 *   description = @Translation("Adds a Tracardi data attribute to desired form elements"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class TracardiWebformHandler extends WebformHandlerBase {

  private const EXCLUDED_ELEMENTS = [
    'webform_actions',
  ];

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Filter out excluded elements
    $elements = array_filter($this->webform->getElementsOriginalDecoded(), static function ($element) {
      return !in_array($element['#type'], self::EXCLUDED_ELEMENTS, FALSE);
    });

    foreach ($elements as $key => $value) {
      $form[$key] = [
        '#type' => 'textfield',
        '#title' => $value['#title'],
        '#default_value' => $this->getSetting($key),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration = $form_state->getValues();
  }

  /**
   * {@inheritdoc}
   */
  public function alterElements(array &$elements, WebformInterface $webform): void {
    foreach (array_filter($this->getSettings()) as $key => $value) {
      $elements[$key]['#tracardi'] = $value;
    }
  }

}

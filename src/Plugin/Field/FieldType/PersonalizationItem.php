<?php

namespace Drupal\tracardi\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\options\Plugin\Field\FieldType\ListStringItem;
use Drupal\tracardi\PersonalizationManager;

/**
 * Defines the 'Personalization' field type.
 *
 * @FieldType(
 *   id = "personalization",
 *   label = @Translation("Personalization"),
 *   description = @Translation("An entity field containing an option list of
 *   personalization plugins."), default_widget = "options_select",
 *   default_formatter = "list_default",
 * )
 */
class PersonalizationItem extends ListStringItem {

  /**
   * @var \Drupal\tracardi\PersonalizationManager
   */
  private PersonalizationManager $pluginManager;

  /**
   * @param \Drupal\Core\TypedData\DataDefinitionInterface $definition
   * @param $name
   * @param \Drupal\Core\TypedData\TypedDataInterface|NULL $parent
   */
  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);
    $this->pluginManager = \Drupal::service('plugin.manager.personalization');
  }

  public static function defaultStorageSettings() {
    return [
      'selected_plugin' => [],
    ] + parent::defaultStorageSettings();
  }

  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $plugins = $this->pluginManager->loadEnabledPlugins();
    $options = [];
    foreach ($plugins as $plugin) {
      $options[$plugin->getId()] = $plugin->label();
    }

    $element['selected_plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Select plugin'),
      '#options' => $options,
      '#default_value' => $this->getSetting('selected_plugin'),
    ];

    return $element;

  }

  /**
   * {@inheritdoc}
   */
  public function getSettableOptions(AccountInterface $account = NULL): array {
    $selectedPlugin = $this->getSetting('selected_plugin');
    $plugin = $this->pluginManager->loadPlugin($selectedPlugin);

    return $plugin->getOptionsList();
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition): array {
    $values['value'] = ['- None -'];
    return $values;
  }

}

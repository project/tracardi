<?php

namespace Drupal\tracardi;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\tracardi\Annotation\Personalization;

class PersonalizationManager extends DefaultPluginManager {

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private ConfigFactoryInterface $configFactory;

  /**
   * @var array
   */
  private array $plugins = [];

  /**
   * @param \Traversable $namespaces
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $configFactory) {
    parent::__construct('Plugin/Personalization', $namespaces, $module_handler, PersonalizationInterface::class, Personalization::class);
    $this->alterInfo('personalization_info');
    $this->setCacheBackend($cache_backend, 'personalization_info');

    $this->configFactory = $configFactory;
  }

  /**
   * @param string $id
   *
   * @return \Drupal\tracardi\PersonalizationInterface
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function loadPlugin(string $id): PersonalizationInterface {
    if (isset($this->plugins[$id])) {
      return $this->plugins[$id];
    }

    $configuration = $this->configFactory->get(PersonalizationBase::CONFIG_BASE . $id)->getRawData();
    $plugin = $this->createInstance($id, $configuration);

    // Set cache and return plugin.
    $this->plugins[$id] = $plugin;
    return $plugin;
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function loadPlugins(): array {
    $definitions = $this->getDefinitions();

    // load plugins.
    return array_map(function ($definition) {
      return $this->loadPlugin($definition['id']);
    }, $definitions);
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function loadEnabledPlugins(): array {
    // Filter enabled ones.
    return array_filter($this->loadPlugins(), static function (PersonalizationInterface $plugin) {
      return $plugin->isEnabled();
    });
  }

}

<?php

namespace Drupal\tracardi\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\tracardi\CookieManager;
use Drupal\tracardi\Event\CookiePreChangeEvent;
use Drupal\tracardi\Exception\ProfileUnknownException;
use Drupal\tracardi\PersonalizationManager;
use Drupal\tracardi\Services\TracardiApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProfileCookiesController extends ControllerBase {

  /**
   * @var \Drupal\tracardi\Services\TracardiApi
   */
  private TracardiApi $apiClient;

  /**
   * @var \Drupal\tracardi\PersonalizationManager
   */
  private PersonalizationManager $pluginManager;

  /**
   * @var \Drupal\tracardi\CookieManager
   */
  private CookieManager $cookieManager;

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private EventDispatcherInterface $eventDispatcher;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * @param \Drupal\tracardi\Services\TracardiApi $apiClient
   * @param \Drupal\tracardi\PersonalizationManager $pluginManager
   * @param \Drupal\tracardi\CookieManager $cookieManager
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(TracardiApi $apiClient, PersonalizationManager $pluginManager, CookieManager $cookieManager, EventDispatcherInterface $eventDispatcher, ConfigFactoryInterface $configFactory) {
    $this->apiClient = $apiClient;
    $this->pluginManager = $pluginManager;
    $this->cookieManager = $cookieManager;
    $this->eventDispatcher = $eventDispatcher;
    $this->config = $configFactory->get('tracardi.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ProfileCookiesController {
    return new static(
      $container->get('tracardi_api'),
      $container->get('plugin.manager.personalization'),
      $container->get('tracardi.cookie_manager'),
      $container->get('event_dispatcher'),
      $container->get('config.factory')
    );
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param string $id
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __invoke(Request $request, string $id): JsonResponse {
    try {
      $profile = $this->apiClient->getProfile($id);
    }
    catch (ProfileUnknownException $e) {
      throw new NotFoundHttpException();
    }

    $response = [];
    /** @var \Drupal\tracardi\PersonalizationInterface $plugin */
    foreach ($this->pluginManager->loadEnabledPlugins() as $plugin) {
      $values = $plugin->calculate($profile);
      sort($values);

      // Send event when values differ.
      $cookieValues = $this->cookieManager->getCookieValues($plugin->getCookieName());
      if ($this->valuesAreDifferent($cookieValues, $values)) {
        $this->eventDispatcher->dispatch(new CookiePreChangeEvent($plugin, $cookieValues, $values));
      }

      $response[$plugin->getCookieName()] = $values;
    }

    // Set A\B testing cookie.
    $cookieName = $this->config->get('ab_cookie_name');
    if ($this->config->get('ab_testing') && !$this->cookieManager->hasCookie($cookieName)) {
      $response[$cookieName] = [random_int(0, 100) < $this->config->get('ab_ratio')];
    }

    return new JsonResponse($response);
  }

  /**
   * @param array $old
   * @param array $new
   *
   * @return bool
   *
   */
  private function valuesAreDifferent(array $old, array $new): bool {
    return array_diff($old, $new) || array_diff($new, $old);
  }

}

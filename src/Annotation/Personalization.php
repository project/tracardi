<?php

namespace Drupal\tracardi\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * @Annotation
 */
class Personalization extends Plugin {

  /**
   * @var string
   */
  public string $description;

  /**
   * @var string
   */
  public string $label;

  /**
   * @var bool
   */
  public bool $configurable;

}

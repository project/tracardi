<?php

namespace Drupal\tracardi\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\tracardi\PersonalizationInterface;

class CookiePreChangeEvent extends Event {

  private PersonalizationInterface $plugin;

  private array $old;

  private array $new;

  /**
   * @param \Drupal\tracardi\PersonalizationInterface $plugin
   * @param array $old
   * @param array $new
   */
  public function __construct(PersonalizationInterface $plugin, array $old, array $new) {
    $this->plugin = $plugin;
    $this->old = $old;
    $this->new = $new;
  }

  /**
   * @return PersonalizationInterface
   */
  public function getPlugin(): PersonalizationInterface {
    return $this->plugin;
  }

  /**
   * @return array
   */
  public function getOld(): array {
    return $this->old;
  }

  /**
   * @return array
   */
  public function getNew(): array {
    return $this->new;
  }

}

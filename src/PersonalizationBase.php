<?php

namespace Drupal\tracardi;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountProxyInterface;
use Tracardi\TracardiPhpSdk\Model\Profile\Profile;

abstract class PersonalizationBase extends PluginBase implements PersonalizationInterface {

  public const CONFIG_BASE = 'tracardi.personalization.';

  /**
   * @var \Drupal\tracardi\CookieManager
   */
  protected CookieManager $cookieManager;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $accountProxy;

  /**
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\tracardi\CookieManager $cookieManager
   * @param \Drupal\Core\Session\AccountProxyInterface $accountProxy
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CookieManager $cookieManager, AccountProxyInterface $accountProxy) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->cookieManager = $cookieManager;
    $this->accountProxy = $accountProxy;

    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function getPageLibraries(): ?array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPageDrupalSettings(): ?array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function getOptionsList(): ?array;

  /**
   * @return array
   */
  public function getPreviewWidget(): array {
    return [
      '#type' => 'select',
      '#title' => $this->label(),
      '#options' => $this->getOptionsList(),
      '#multiple' => TRUE,
      '#size' => 5,
    ];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function getCookieName(): ?string;

  /**
   * {@inheritdoc}
   */
  abstract public function calculate(Profile $profile): array;

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): MarkupInterface {
    return $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function isConfigurable(): bool {
    return $this->pluginDefinition['configurable'] ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return $this->configuration['enabled'];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'enabled' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    return [
      '#type' => 'details',
      '#title' => $this->label(),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#group' => 'plugins',
      '#tree' => TRUE,
      'enabled' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable plugin'),
        '#description' => $this->getDescription(),
        '#default_value' => $this->isEnabled(),
        '#required' => FALSE,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {}

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = array_merge($this->defaultConfiguration(), $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigName(): string {
    return self::CONFIG_BASE . $this->getId();
  }

}

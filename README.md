# Description
This module integrates your Drupal website with the Tracardi customer data platform and allows the administering of content based on customer profile data.

# Installation
### Install the module via Composer:
    'composer require drupal/tracardi --update-with-dependencies'

### Configure and establish connection with the Tracardi platform in the back-end of your Drupal website:
     '/admin/config/tracardi'

### Add field(s) to Entity type(s):
![Add field example](https://i.imgur.com/H6weA6o.png)

# Usage
## Configuration
### Tracardi API
Authenticate to the Tracardi API by filling in your credentials.
### Scripts
Enable the script via the module or load it externally.
### Events
Decide which events should be enabled and sent to Tracardi.
### Preview
Enabling the preview option provides you with a widget to test your Personalization plugin(s).  
![Preview widget preview](https://imgur.com/0yGq5Qc.png)

# Extendibility
### Plugins
The purpose of the 'Segments' plugin is to set a cookie that contains the profile segments of the user.  
Other, custom Personalization plugins can be added at will and will be dynamically added to the configuration of the module.
### PHP SDK
The Tracardi PHP SDK dependency allows this module to be used to directly retrieve profile information from Tracardi in a secure way.

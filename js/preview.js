(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.tracardi_preview = {
    attach: function (context) {
      $('body').once('tracardi-preview').each(function () {
        Drupal.behaviors.tracardi_preview.init(context);
      })
    },

    init: function (context) {
      // Build widget from HTML.
      var $form = $(context).find('#js--tracardi-preview');

      // Select values from cookies.
      $form.find('.widget').each(function () {
        var $widget = $(this);
        var cookieName = $widget.data('cookie-name');
        $.each(Drupal.behaviors.tracardi.getCookie(cookieName), function(key, value){
          $widget.find("option[value='" + value + "']").prop('selected', true).trigger('change');
        });
      });

      // Save selection to cookie on submit.
      $form.on('submit', function (e) {
        $form.find('.widget').each(function () {
          var $widget = $(this);
          var cookieName = $widget.data('cookie-name');
          var checked = $widget[0].querySelectorAll(':checked');
          var selected = [...checked].map(option => option.value);
          Drupal.behaviors.tracardi.setCookie(cookieName, selected);
        })
      });

      // Add form to the page.
      $('body').append($form);
    }
  };

}(jQuery, Drupal, drupalSettings));
